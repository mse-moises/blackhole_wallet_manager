import 'package:blackhole_wallet_manager/core/error/failures.dart';
import 'package:blackhole_wallet_manager/features/tag/domain/entities/tag_entity.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/entities/transaction_entity.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/repository/transaction_repository.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/usecases/create_transaction_usecase.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../helpers/transaction_mocks_test.dart';
import '../../helpers/transaction_mocks_test.mocks.dart';

void main() {
  late CreateTransactionUseCase usecase;
  late MockTransactionRepository transactionRepository;

  final CreateTransactionParams createTransactionParams =
      getGenericParamsTransaction();
  final TransactionEntity transactionEntityTest = createTransactionParams;
  final DeviceFailure failureTest = DeviceFailure();

  setUp(() {
    transactionRepository = MockTransactionRepository();

    usecase = CreateTransactionUseCase(repository: transactionRepository);
  });

  setCreateTransactionSuccess() {
    when(transactionRepository.createTransaction(any))
        .thenAnswer((realInvocation) async => Right(transactionEntityTest));
  }

  setCreateTransactionFail() {
    when(transactionRepository.createTransaction(any))
        .thenAnswer((realInvocation) async => Left(failureTest));
  }

  Future<Either<Failure, TransactionEntity>> callUseCase() {
    return usecase(createTransactionParams);
  }

  group(CreateTransactionUseCase, () {
    test("return a transaction entity", () async {
      setCreateTransactionSuccess();
      final response = await callUseCase();
      expect(response, Right(transactionEntityTest));
    });
    test("return a device failure", () async {
      setCreateTransactionFail();
      final response = await callUseCase();
      expect(response, Left(failureTest));
    });
  });
}
