import 'package:blackhole_wallet_manager/features/transaction/domain/entities/transaction_entity.dart';
import 'package:blackhole_wallet_manager/core/error/failures.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/usecases/delete_transaction_usecase.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../helpers/transaction_mocks_test.dart';
import '../../helpers/transaction_mocks_test.mocks.dart';

void main() {
  late DeleteTransactionUseCase usecase;
  late MockTransactionRepository repository;

  final TransactionEntity transactionEntityTest = getGenericTransactionEntity();

  setUp(() {
    repository = MockTransactionRepository();
    usecase = DeleteTransactionUseCase(repository: repository);
  });

  setDeleteTransactionSuccess() {
    when(repository.deleteTransaction(any))
        .thenAnswer((realInvocation) async => Right(transactionEntityTest));
  }

  setDeleteTransactionFail() {
    when(repository.deleteTransaction(any))
        .thenAnswer((realInvocation) async => Left(DeviceFailure()));
  }

  Future<Either<Failure, TransactionEntity>> callUseCase() {
    return usecase(getGenericParamsDeleteTransaction());
  }

  group(DeleteTransactionUseCase, () {
    test("return a transaction entity", () async {
      setDeleteTransactionSuccess();
      final result = await callUseCase();
      expect(result, Right(transactionEntityTest));
    });
    test("return a device failure", () async {
      setDeleteTransactionFail();
      final result = await callUseCase();
      expect(result, Left(DeviceFailure()));
    });
  });
}
