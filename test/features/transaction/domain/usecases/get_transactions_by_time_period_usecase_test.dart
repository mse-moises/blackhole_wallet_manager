import 'package:blackhole_wallet_manager/core/error/failures.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/entities/transaction_entity.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/usecases/get_transactions_by_time_period_usecase.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../helpers/transaction_mocks_test.dart';
import '../../helpers/transaction_mocks_test.mocks.dart';

void main() {
  late GetTransactionsByTimePeriodUseCase usecase;
  late MockTransactionRepository repository;

  List<TransactionEntity> listTransactionTest = <TransactionEntity>[
    getGenericTransactionEntity(),
    getGenericTransactionEntity(),
  ];

  setUp(() {
    repository = MockTransactionRepository();
    usecase = GetTransactionsByTimePeriodUseCase(repository: repository);
  });

  callUseCase() {
    return usecase(const GetTransactionsByTimePeriodParams());
  }

  setGetTransactionsByTimePeriodSucess() {
    when(repository.getTransactionsByTimePeriod(any))
        .thenAnswer((realInvocation) async => Right(listTransactionTest));
  }

  setGetTransactionsByTimePeriodFail() {
    when(repository.getTransactionsByTimePeriod(any))
        .thenAnswer((realInvocation) async => Left(DeviceFailure()));
  }

  group(GetTransactionsByTimePeriodUseCase, () {
    test("return a transaction list", () async {
      setGetTransactionsByTimePeriodSucess();
      final response = await callUseCase();
      expect(response, Right(listTransactionTest));
    });

    test("return a device failure", () async {
      setGetTransactionsByTimePeriodFail();
      final response = await callUseCase();
      expect(response, Left(DeviceFailure()));
    });
  });
}
