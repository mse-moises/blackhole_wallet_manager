import 'package:blackhole_wallet_manager/features/transaction/domain/entities/transaction_entity.dart';
import 'package:blackhole_wallet_manager/core/error/failures.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/usecases/edit_transaction_usecase.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../helpers/transaction_mocks_test.dart';
import '../../helpers/transaction_mocks_test.mocks.dart';

void main() {
  late EditTransactionUseCase usecase;
  late MockTransactionRepository repository;

  final TransactionEntity transactionEntityTest = getGenericTransactionEntity();

  setUp(() {
    repository = MockTransactionRepository();
    usecase = EditTransactionUseCase(repository: repository);
  });

  setEditTransactionSuccess() {
    when(repository.editTransaction(any))
        .thenAnswer((_) async => Right(transactionEntityTest));
  }

  setEditTransactionFail() {
    when(repository.editTransaction(any))
        .thenAnswer((_) async => Left(DeviceFailure()));
  }

  Future<Either<Failure, TransactionEntity>> callUseCase() {
    return usecase(getGenericParamsEditTransaction());
  }

  group(EditTransactionUseCase, () {
    test("return a transaction entity", () async {
      setEditTransactionSuccess();
      final result = await callUseCase();
      expect(result, Right(transactionEntityTest));
    });
    test("return a device failure", () async {
      setEditTransactionFail();
      final result = await callUseCase();
      expect(result, Left(DeviceFailure()));
    });
  });
}
