import 'package:blackhole_wallet_manager/features/transaction/data/datasource/transaction_datasource.dart';
import 'package:blackhole_wallet_manager/features/transaction/data/model/transaction_model.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/entities/transaction_entity.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/repository/transaction_repository.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/usecases/create_transaction_usecase.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/usecases/delete_transaction_usecase.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/usecases/edit_transaction_usecase.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/usecases/get_transactions_by_time_period_usecase.dart';
import 'package:mockito/annotations.dart';

import '../../tag/helpers/tag_mocks_test.dart';

@GenerateMocks([TransactionRepository, TransactionDataSource])
void main() {}

const double transactionAmoutTest = 100;
final DateTime transactionDateTest = DateTime(2022);
const String transactionDescriptionTest = "description";
const String transactionTitleTest = "description";

TransactionModel getGenericTransactionModel() {
  return TransactionModel(
    amount: transactionAmoutTest,
    date: transactionDateTest,
    description: transactionDescriptionTest,
    tag: getGenericTagEntity(),
    title: transactionTitleTest,
  );
}

TransactionEntity getGenericTransactionEntity() {
  return getGenericTransactionModel();
}

CreateTransactionParams getGenericParamsTransaction() {
  return CreateTransactionParams(
    amount: transactionAmoutTest,
    date: transactionDateTest,
    description: transactionDescriptionTest,
    tag: getGenericTagEntity(),
    title: transactionTitleTest,
  );
}

GetTransactionsByTimePeriodParams getGenericParamsTransactionsByTimePeriod() {
  return const GetTransactionsByTimePeriodParams();
}

DeleteTransactionParams getGenericParamsDeleteTransaction() {
  return DeleteTransactionParams(transaction: getGenericTransactionEntity());
}

EditTransactionParams getGenericParamsEditTransaction() {
  return EditTransactionParams(
    originalTransaction: getGenericTransactionEntity(),
    transactionEdited: getGenericTransactionEntity(),
  );
}
