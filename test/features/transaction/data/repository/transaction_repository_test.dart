import 'package:blackhole_wallet_manager/core/error/expections.dart';
import 'package:blackhole_wallet_manager/core/error/failures.dart';
import 'package:blackhole_wallet_manager/features/transaction/data/model/transaction_model.dart';
import 'package:blackhole_wallet_manager/features/transaction/data/repository/transaction_repository_implementation.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/entities/transaction_entity.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/repository/transaction_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../helpers/transaction_mocks_test.dart';
import '../../helpers/transaction_mocks_test.mocks.dart';

void main() {
  late TransactionRepositoryImplementation repository;
  late MockTransactionDataSource datasource;

  final TransactionModel transactionModel = getGenericTransactionModel();

  final List<TransactionModel> transactionModelListTest = <TransactionModel>[
    getGenericTransactionModel(),
    getGenericTransactionModel(),
  ];

  setUp(() {
    datasource = MockTransactionDataSource();
    repository = TransactionRepositoryImplementation(datasource: datasource);
  });

  setCreateTransactionSuccess() {
    when(datasource.createTransaction(any))
        .thenAnswer((realInvocation) async => transactionModel);
  }

  setCreateTransactionFail() {
    when(datasource.createTransaction(any)).thenThrow(DeviceException());
  }

  Future<Either<Failure, TransactionModel>> callCreateTransaction() {
    return repository.createTransaction(getGenericParamsTransaction());
  }

  setTransactionsByTimePeriodSuccess() {
    when(datasource.getTransactionsByTimePeriod(any))
        .thenAnswer((realInvocation) async => transactionModelListTest);
  }

  setTransactionsByTimePeriodFail() {
    when(datasource.getTransactionsByTimePeriod(any))
        .thenThrow(DeviceException());
  }

  Future<Either<Failure, List<TransactionEntity>>>
      callGetTransactionsByTimePeriod() {
    return repository.getTransactionsByTimePeriod(
        getGenericParamsTransactionsByTimePeriod());
  }

  setDeleteTransactionSuccess() {
    when(datasource.deleteTransaction(any))
        .thenAnswer((realInvocation) async => transactionModel);
  }

  setDeleteTransactionFail() {
    when(datasource.deleteTransaction(any)).thenThrow(DeviceException());
  }

  Future<Either<Failure, TransactionEntity>> callDeleteTransaction() {
    return repository.deleteTransaction(getGenericParamsDeleteTransaction());
  }

  setEditTransactionSuccess() {
    when(datasource.editTransaction(any))
        .thenAnswer((realInvocation) async => transactionModel);
  }

  setEditTransactionFail() {
    when(datasource.editTransaction(any)).thenThrow(DeviceException());
  }

  Future<Either<Failure, TransactionEntity>> callEditTransaction() {
    return repository.editTransaction(getGenericParamsEditTransaction());
  }

  group(TransactionRepository, () {
    group("create transaction method", () {
      test("return a transaction entity", () async {
        setCreateTransactionSuccess();
        final result = await callCreateTransaction();
        expect(result, Right(getGenericTransactionModel()));
      });

      test("return a device failure", () async {
        setCreateTransactionFail();
        final result = await callCreateTransaction();
        expect(result, Left(DeviceFailure()));
      });
    });

    group("get transactions by time period method", () {
      test("return a transaction list", () async {
        setTransactionsByTimePeriodSuccess();
        final result = await callGetTransactionsByTimePeriod();
        expect(result, Right(transactionModelListTest));
      });

      test("return a device failure", () async {
        setTransactionsByTimePeriodFail();
        final result = await callGetTransactionsByTimePeriod();
        expect(result, Left(DeviceFailure()));
      });
    });

    group("delete transaction method", () {
      test("return a transaction entity", () async {
        setDeleteTransactionSuccess();
        final result = await callDeleteTransaction();
        expect(result, Right(transactionModel));
      });

      test("return a device failure", () async {
        setDeleteTransactionFail();
        final result = await callDeleteTransaction();
        expect(result, Left(DeviceFailure()));
      });
    });

    group("edit transaction method", () {
      test("return a transaction entity", () async {
        setEditTransactionSuccess();
        final result = await callEditTransaction();
        expect(result, Right(transactionModel));
      });

      test("return a device failure", () async {
        setEditTransactionFail();
        final result = await callEditTransaction();
        expect(result, Left(DeviceFailure()));
      });
    });
  });
}
