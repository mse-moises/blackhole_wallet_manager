import 'package:blackhole_wallet_manager/features/tag/data/datasource/tag_local_datasource.dart';
import 'package:blackhole_wallet_manager/features/tag/data/model/tag_model.dart';
import 'package:blackhole_wallet_manager/features/tag/domain/entities/tag_entity.dart';
import 'package:blackhole_wallet_manager/features/tag/domain/repository/tag_repository_interface.dart';
import 'package:blackhole_wallet_manager/features/tag/domain/usecases/delete_tag_usecase.dart';
import 'package:blackhole_wallet_manager/features/tag/domain/usecases/edit_tag_usecase.dart';
import 'package:mockito/annotations.dart';

@GenerateMocks([TagRepository, TagLocalDataSource])
void main() {}

const genericTagTestName = "test";

TagModel getGenericTagModel() {
  return const TagModel(title: "test");
}

TagEntity getGenericTagEntity() {
  return getGenericTagModel();
}

EditTagParams getGenericParamsEditTag() {
  return EditTagParams(
    originalTag: getGenericTagEntity(),
    editedTag: getGenericTagEntity(),
  );
}

DeleteTagParams getGenericParamsDeleteTag() {
  return DeleteTagParams(
    tag: getGenericTagEntity(),
  );
}
