import 'package:blackhole_wallet_manager/core/error/expections.dart';
import 'package:blackhole_wallet_manager/core/error/failures.dart';
import 'package:blackhole_wallet_manager/features/tag/data/model/tag_model.dart';
import 'package:blackhole_wallet_manager/features/tag/data/repository/tag_repository_implementation.dart';

import 'package:blackhole_wallet_manager/features/tag/domain/repository/tag_repository_interface.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../helpers/tag_mocks_test.dart';
import '../../helpers/tag_mocks_test.mocks.dart';

void main() {
  late TagRepository repository;
  late MockTagLocalDataSource mockDatasource;

  const testTagTitle = "test";
  final testTagModel = TagModel(title: testTagTitle);

  final testListTagModel = <TagModel>[
    getGenericTagModel(),
    getGenericTagModel(),
  ];

  setUp(() {
    mockDatasource = MockTagLocalDataSource();
    repository = TagRepositoryImplementation(localDataSource: mockDatasource);
  });

  setCreateTagSuccess() {
    when(mockDatasource.createTag(any)).thenAnswer((_) async => testTagModel);
  }

  setCreateTagFail() {
    when(mockDatasource.createTag(any)).thenThrow(DeviceException());
  }

  callRepositoryCreateTag() async {
    return repository.createTag(testTagTitle);
  }

  setGetAllTagsSuccess() {
    when(mockDatasource.getAllTags()).thenAnswer((_) async => testListTagModel);
  }

  setGetAllTagsFail() {
    when(mockDatasource.getAllTags()).thenThrow(DeviceException());
  }

  callRepositoryGetAllTags() async {
    return repository.getAllTags();
  }

  setEditTagSuccess() {
    when(mockDatasource.editTag(any))
        .thenAnswer((_) async => getGenericTagModel());
  }

  setEditTagFail() {
    when(mockDatasource.editTag(any)).thenThrow(DeviceException());
  }

  callEditTag() async {
    return repository.editTag(getGenericParamsEditTag());
  }

  setDeleteTagSuccess() {
    when(mockDatasource.deleteTag(any))
        .thenAnswer((_) async => getGenericTagModel());
  }

  setDeleteTagFail() {
    when(mockDatasource.deleteTag(any)).thenThrow(DeviceException());
  }

  callDeleteTag() async {
    return repository.deleteTag(getGenericTagEntity());
  }

  group(TagRepository, () {
    group("create tag method", () {
      test("return a tag model", () async {
        setCreateTagSuccess();
        final result = await callRepositoryCreateTag();
        expect(result, Right(testTagModel));
      });

      test("return a device failure", () async {
        setCreateTagFail();
        final result = await callRepositoryCreateTag();
        expect(result, Left(DeviceFailure()));
      });
    });

    group("get all tags method", () {
      test("return a tag list", () async {
        setGetAllTagsSuccess();
        final result = await callRepositoryGetAllTags();
        expect(result, Right(testListTagModel));
      });
      test("return a device failure", () async {
        setGetAllTagsFail();
        final result = await callRepositoryGetAllTags();
        expect(result, Left(DeviceFailure()));
      });
    });

    group("edit tag method", () {
      test("return a tag model", () async {
        setEditTagSuccess();
        final result = await callEditTag();
        expect(result, Right(getGenericTagModel()));
      });
      test("return a device failure", () async {
        setEditTagFail();
        final result = await callEditTag();
        expect(result, Left(DeviceFailure()));
      });
    });
    group("delete tag method", () {
      test("return a tag model", () async {
        setDeleteTagSuccess();
        final result = await callDeleteTag();
        expect(result, Right(getGenericTagModel()));
      });
      test("return a device failure", () async {
        setDeleteTagFail();
        final result = await callDeleteTag();
        expect(result, Left(DeviceFailure()));
      });
    });
  });
}
