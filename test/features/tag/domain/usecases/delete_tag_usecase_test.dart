import 'package:blackhole_wallet_manager/core/error/failures.dart';
import 'package:blackhole_wallet_manager/features/tag/domain/usecases/delete_tag_usecase.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../helpers/tag_mocks_test.dart';
import '../../helpers/tag_mocks_test.mocks.dart';

void main() {
  late DeleteTagUseCase usecase;
  late MockTagRepository repository;
  setUp(() {
    repository = MockTagRepository();
    usecase = DeleteTagUseCase(repository: repository);
  });

  setUpDeleteTagSuccess() {
    when(repository.deleteTag(any))
        .thenAnswer((_) async => Right(getGenericTagEntity()));
  }

  setUpDeleteTagFail() {
    when(repository.deleteTag(any))
        .thenAnswer((_) async => Left(DeviceFailure()));
  }

  callDeleteTagUseCase() {
    return usecase(getGenericParamsDeleteTag());
  }

  group(DeleteTagUseCase, () {
    test("return a tag entity", () async {
      setUpDeleteTagSuccess();
      final result = await callDeleteTagUseCase();
      expect(result, Right(getGenericTagEntity()));
    });
    test("return a device failure", () async {
      setUpDeleteTagFail();
      final result = await callDeleteTagUseCase();
      expect(result, Left(DeviceFailure()));
    });
  });
}
