import 'package:blackhole_wallet_manager/core/error/failures.dart';
import 'package:blackhole_wallet_manager/features/tag/domain/repository/tag_repository_interface.dart';
import 'package:blackhole_wallet_manager/features/tag/domain/usecases/edit_tag_usecase.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../helpers/tag_mocks_test.dart';
import '../../helpers/tag_mocks_test.mocks.dart';

void main() {
  late EditTagUseCase editTagUseCase;
  late MockTagRepository repository;

  setUp(() {
    repository = MockTagRepository();
    editTagUseCase = EditTagUseCase(repository: repository);
  });

  callEditTagUseCase() {
    return editTagUseCase(getGenericParamsEditTag());
  }

  setUpEditTagSuccess() {
    when(repository.editTag(any))
        .thenAnswer((_) async => Right(getGenericTagEntity()));
  }

  setUpEditTagFail() {
    when(repository.editTag(any))
        .thenAnswer((_) async => Left(DeviceFailure()));
  }

  group(EditTagUseCase, () {
    test("return a tag entity", () async {
      setUpEditTagSuccess();
      final result = await callEditTagUseCase();
      expect(result, Right(getGenericTagEntity()));
    });
    test("return a device failure", () async {
      setUpEditTagFail();
      final result = await callEditTagUseCase();
      expect(result, Left(DeviceFailure()));
    });
  });
}
