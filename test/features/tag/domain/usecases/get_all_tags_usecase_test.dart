import 'package:blackhole_wallet_manager/core/error/failures.dart';
import 'package:blackhole_wallet_manager/features/tag/domain/entities/tag_entity.dart';
import 'package:blackhole_wallet_manager/features/tag/domain/usecases/get_all_tags_usecase.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../helpers/tag_mocks_test.dart';
import '../../helpers/tag_mocks_test.mocks.dart';

void main() {
  late GetAllTagsUseCase usecase;
  late MockTagRepository mockTagRepository;

  final testListTagEntity = <TagEntity>[
    getGenericTagEntity(),
    getGenericTagEntity(),
  ];

  setUp(() {
    mockTagRepository = MockTagRepository();
    usecase = GetAllTagsUseCase(repository: mockTagRepository);
  });

  setGetAllTagsSuccess() {
    when(mockTagRepository.getAllTags())
        .thenAnswer((_) async => Right(testListTagEntity));
  }

  setGetAllTagsFail() {
    when(mockTagRepository.getAllTags())
        .thenAnswer((_) async => Left(DeviceFailure()));
  }

  callUseCase() {
    return usecase(const GetAllTagsParams());
  }

  group(GetAllTagsUseCase, () {
    test("return a tag list", () async {
      setGetAllTagsSuccess();
      final result = await callUseCase();
      expect(result, Right(testListTagEntity));
    });
    test("return a device failure", () async {
      setGetAllTagsFail();
      final result = await callUseCase();
      expect(result, Left(DeviceFailure()));
    });
  });
}
