import 'package:blackhole_wallet_manager/core/error/failures.dart';
import 'package:blackhole_wallet_manager/features/tag/domain/entities/tag_entity.dart';
import 'package:blackhole_wallet_manager/features/tag/domain/usecases/create_tag_usecase.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../helpers/tag_mocks_test.dart';
import '../../helpers/tag_mocks_test.mocks.dart';

void main() {
  late MockTagRepository mockRepository;
  late CreateTagUseCase createTagUsecase;

  final testTagEntity = getGenericTagEntity();

  setUp(() {
    mockRepository = MockTagRepository();
    createTagUsecase = CreateTagUseCase(repository: mockRepository);
  });

  setCreateTagSuccess() {
    when(mockRepository.createTag(any))
        .thenAnswer((_) async => Right(testTagEntity));
  }

  setCreateTagFail() {
    when(mockRepository.createTag(any))
        .thenAnswer((_) async => Left(DeviceFailure()));
  }

  callUseCase() async {
    return createTagUsecase(const CreateTagParams(title: genericTagTestName));
  }

  group(CreateTagUseCase, () {
    test("return a tag entity", () async {
      setCreateTagSuccess();
      final response = await callUseCase();
      expect(response, Right(testTagEntity));
    });

    test("return a device failure", () async {
      setCreateTagFail();
      final response = await callUseCase();
      expect(response, Left(DeviceFailure()));
    });
  });
}
