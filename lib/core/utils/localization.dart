import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter/material.dart';

AppLocalizations getAppLocalization(BuildContext context) =>
    AppLocalizations.of(context)!;
