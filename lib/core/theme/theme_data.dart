import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

final mainThemeData = ThemeData(
  primarySwatch: Colors.purple,
  textTheme: TextTheme(
    headline5: GoogleFonts.nunito(
      textStyle: const TextStyle(
        fontWeight: FontWeight.bold,
        color: Colors.black,
        fontSize: 24,
      ),
    ),
    bodyText2: GoogleFonts.nunito(
      textStyle: const TextStyle(
        fontWeight: FontWeight.bold,
        color: Colors.black,
        fontSize: 20,
      ),
    ),
    bodyText1: GoogleFonts.nunito(
      textStyle: const TextStyle(
        fontWeight: FontWeight.normal,
        color: Colors.black,
        fontSize: 18,
      ),
    ),
    subtitle2: GoogleFonts.nunito(
        textStyle: const TextStyle(
      fontWeight: FontWeight.normal,
      color: Colors.black,
      fontSize: 14,
    )),
  ),
);

final mainBoxShadow = BoxShadow(
  color: Colors.black.withOpacity(0.8),
  spreadRadius: 1,
  blurRadius: 1,
  offset: const Offset(3, 3), // changes position of shadow
);

final lightColor = Colors.grey[200];
