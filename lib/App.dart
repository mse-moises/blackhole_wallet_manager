import 'package:blackhole_wallet_manager/core/theme/theme_data.dart';
import 'package:blackhole_wallet_manager/pages/home/presentation/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';

class BlackHoleApp extends StatelessWidget {
  const BlackHoleApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "BlackHole",
      theme: mainThemeData,
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      home: const HomePage(),
    );
  }
}
