import 'package:blackhole_wallet_manager/core/utils/localization.dart';
import 'package:blackhole_wallet_manager/pages/home/presentation/components/main_card.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final localization = getAppLocalization(context);

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("XXXX"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const MainCard(),
              const Divider(),
              Text(localization.lastExpenditures),
            ],
          ),
        ),
      ),
    );
  }
}
