import 'package:blackhole_wallet_manager/core/theme/designt_constants.dart';
import 'package:blackhole_wallet_manager/core/theme/theme_data.dart';
import 'package:blackhole_wallet_manager/core/utils/localization.dart';
import 'package:flutter/material.dart';

class MainCard extends StatelessWidget {
  const MainCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final localization = getAppLocalization(context);

    final ThemeData _theme = Theme.of(context);
    return Container(
      decoration: BoxDecoration(
        boxShadow: [
          mainBoxShadow,
        ],
        color: _theme.primaryColor,
        borderRadius: BorderRadiusDirectional.circular(2),
      ),
      height: 150,
      width: double.infinity,
      child: Padding(
        padding: const EdgeInsets.all(veryLargeSpace),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              localization.balance,
              style: _theme.textTheme.headline6?.copyWith(
                color: lightColor,
              ),
            ),
            Text(
              "R\$00.00",
              style: _theme.textTheme.headline3?.copyWith(
                color: lightColor,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
