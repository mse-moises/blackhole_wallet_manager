import 'package:blackhole_wallet_manager/core/error/failures.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/entities/transaction_entity.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/usecases/create_transaction_usecase.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/usecases/delete_transaction_usecase.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/usecases/edit_transaction_usecase.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/usecases/get_transactions_by_time_period_usecase.dart';
import 'package:dartz/dartz.dart';

abstract class TransactionRepository {
  Future<Either<Failure, TransactionEntity>> createTransaction(
      CreateTransactionParams transactionParams);

  Future<Either<Failure, List<TransactionEntity>>> getTransactionsByTimePeriod(
      GetTransactionsByTimePeriodParams timePeriod);

  Future<Either<Failure, TransactionEntity>> deleteTransaction(
      DeleteTransactionParams transaction);

  Future<Either<Failure, TransactionEntity>> editTransaction(
      EditTransactionParams transaction);
}
