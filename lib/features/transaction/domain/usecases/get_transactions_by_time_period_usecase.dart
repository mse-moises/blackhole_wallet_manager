import 'package:blackhole_wallet_manager/core/error/failures.dart';
import 'package:blackhole_wallet_manager/core/usecases/usecase_interface.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/entities/transaction_entity.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/repository/transaction_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

class GetTransactionsByTimePeriodUseCase
    implements
        UseCase<List<TransactionEntity>, GetTransactionsByTimePeriodParams> {
  final TransactionRepository repository;

  GetTransactionsByTimePeriodUseCase({required this.repository});
  @override
  Future<Either<Failure, List<TransactionEntity>>> call(
      GetTransactionsByTimePeriodParams params) {
    return repository.getTransactionsByTimePeriod(params);
  }
}

class GetTransactionsByTimePeriodParams extends Equatable {
  final DateTime? startAt;
  final DateTime? endedAt;

  const GetTransactionsByTimePeriodParams({this.startAt, this.endedAt});
  @override
  List<Object?> get props => [startAt, endedAt];
}
