import 'package:blackhole_wallet_manager/core/error/failures.dart';
import 'package:blackhole_wallet_manager/core/usecases/usecase_interface.dart';
import 'package:blackhole_wallet_manager/features/tag/domain/entities/tag_entity.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/entities/transaction_entity.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/repository/transaction_repository.dart';
import 'package:dartz/dartz.dart';

class CreateTransactionUseCase
    implements UseCase<TransactionEntity, CreateTransactionParams> {
  final TransactionRepository repository;

  CreateTransactionUseCase({required this.repository});
  @override
  Future<Either<Failure, TransactionEntity>> call(
      CreateTransactionParams params) {
    return repository.createTransaction(params);
  }
}

class CreateTransactionParams extends TransactionEntity {
  const CreateTransactionParams({
    required DateTime date,
    required String description,
    required double amount,
    required TagEntity tag,
    required String title,
  }) : super(
          date: date,
          description: description,
          amount: amount,
          tag: tag,
          title: title,
        );
}
