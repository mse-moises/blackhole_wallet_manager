import 'package:blackhole_wallet_manager/core/error/failures.dart';
import 'package:blackhole_wallet_manager/core/usecases/usecase_interface.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/entities/transaction_entity.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/repository/transaction_repository.dart';

import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

class DeleteTransactionUseCase
    implements UseCase<TransactionEntity, DeleteTransactionParams> {
  final TransactionRepository repository;

  DeleteTransactionUseCase({required this.repository});
  @override
  Future<Either<Failure, TransactionEntity>> call(
      DeleteTransactionParams deleteTransactionParams) {
    return repository.deleteTransaction(deleteTransactionParams);
  }
}

class DeleteTransactionParams extends Equatable {
  final TransactionEntity transaction;

  const DeleteTransactionParams({required this.transaction});
  @override
  List<Object?> get props => [transaction];
}
