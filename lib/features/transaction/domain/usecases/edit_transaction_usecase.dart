import 'package:blackhole_wallet_manager/core/error/failures.dart';
import 'package:blackhole_wallet_manager/core/usecases/usecase_interface.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/entities/transaction_entity.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/repository/transaction_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

class EditTransactionUseCase
    implements UseCase<TransactionEntity, EditTransactionParams> {
  final TransactionRepository repository;

  EditTransactionUseCase({required this.repository});
  @override
  Future<Either<Failure, TransactionEntity>> call(
      EditTransactionParams params) {
    return repository.editTransaction(params);
  }
}

class EditTransactionParams extends Equatable {
  final TransactionEntity originalTransaction;
  final TransactionEntity transactionEdited;

  const EditTransactionParams({
    required this.originalTransaction,
    required this.transactionEdited,
  });
  @override
  // TODO: implement props
  List<Object?> get props => [originalTransaction, transactionEdited];
}
