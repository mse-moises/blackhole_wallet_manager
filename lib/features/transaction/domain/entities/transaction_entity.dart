import 'package:blackhole_wallet_manager/features/tag/domain/entities/tag_entity.dart';
import 'package:equatable/equatable.dart';

class TransactionEntity extends Equatable {
  final String title;
  final String description;
  final double amount;
  final TagEntity tag;
  final DateTime date;

  const TransactionEntity({
    required this.date,
    required this.description,
    required this.amount,
    required this.tag,
    required this.title,
  });

  @override
  List<Object?> get props => [title, amount, tag, date];
}
