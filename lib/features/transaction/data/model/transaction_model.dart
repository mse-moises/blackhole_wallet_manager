import 'package:blackhole_wallet_manager/features/tag/domain/entities/tag_entity.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/entities/transaction_entity.dart';

class TransactionModel extends TransactionEntity {
  const TransactionModel({
    required DateTime date,
    required String description,
    required double amount,
    required TagEntity tag,
    required String title,
  }) : super(
          date: date,
          description: description,
          amount: amount,
          tag: tag,
          title: title,
        );
}
