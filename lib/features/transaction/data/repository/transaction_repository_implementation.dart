import 'package:blackhole_wallet_manager/core/error/expections.dart';
import 'package:blackhole_wallet_manager/features/transaction/data/datasource/transaction_datasource.dart';
import 'package:blackhole_wallet_manager/features/transaction/data/model/transaction_model.dart';
import 'package:blackhole_wallet_manager/core/error/failures.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/entities/transaction_entity.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/repository/transaction_repository.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/usecases/delete_transaction_usecase.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/usecases/edit_transaction_usecase.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/usecases/get_transactions_by_time_period_usecase.dart';
import 'package:dartz/dartz.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/usecases/create_transaction_usecase.dart';

class TransactionRepositoryImplementation implements TransactionRepository {
  final TransactionDataSource datasource;

  TransactionRepositoryImplementation({required this.datasource});

  @override
  Future<Either<Failure, TransactionModel>> createTransaction(
      CreateTransactionParams transactionParams) async {
    try {
      final remoteAddress =
          await datasource.createTransaction(transactionParams);

      return Right(remoteAddress);
    } on DeviceException {
      return Left(DeviceFailure());
    }
  }

  @override
  Future<Either<Failure, List<TransactionEntity>>> getTransactionsByTimePeriod(
      GetTransactionsByTimePeriodParams timePeriod) async {
    try {
      final transactionList =
          await datasource.getTransactionsByTimePeriod(timePeriod);

      return Right(transactionList);
    } on DeviceException {
      return Left(DeviceFailure());
    }
  }

  @override
  Future<Either<Failure, TransactionEntity>> deleteTransaction(
      DeleteTransactionParams deleteTransactionParams) async {
    try {
      final transactionDeleted =
          await datasource.deleteTransaction(deleteTransactionParams);

      return Right(transactionDeleted);
    } on DeviceException {
      return Left(DeviceFailure());
    }
  }

  @override
  Future<Either<Failure, TransactionEntity>> editTransaction(
      EditTransactionParams editTransaction) async {
    try {
      final transactionEdited =
          await datasource.editTransaction(editTransaction);

      return Right(transactionEdited);
    } on DeviceException {
      return Left(DeviceFailure());
    }
  }
}
