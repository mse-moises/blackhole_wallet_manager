import 'package:blackhole_wallet_manager/features/transaction/data/model/transaction_model.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/entities/transaction_entity.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/usecases/create_transaction_usecase.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/usecases/delete_transaction_usecase.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/usecases/edit_transaction_usecase.dart';
import 'package:blackhole_wallet_manager/features/transaction/domain/usecases/get_transactions_by_time_period_usecase.dart';

abstract class TransactionDataSource {
  Future<TransactionModel> createTransaction(CreateTransactionParams params);
  Future<List<TransactionModel>> getTransactionsByTimePeriod(
      GetTransactionsByTimePeriodParams transactionParams);
  Future<TransactionModel> deleteTransaction(
      DeleteTransactionParams deleteTransactionParams);
  Future<TransactionModel> editTransaction(
      EditTransactionParams editTransactionParams);
}
