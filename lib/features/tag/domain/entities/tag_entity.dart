import 'package:equatable/equatable.dart';

class TagEntity extends Equatable {
  final String title;

  const TagEntity({required this.title});

  @override
  List<Object?> get props => [title];
}
