import 'package:blackhole_wallet_manager/core/error/failures.dart';
import 'package:blackhole_wallet_manager/features/tag/domain/entities/tag_entity.dart';
import 'package:blackhole_wallet_manager/features/tag/domain/usecases/edit_tag_usecase.dart';
import 'package:dartz/dartz.dart';

abstract class TagRepository {
  Future<Either<Failure, TagEntity>> createTag(String title);
  Future<Either<Failure, TagEntity>> editTag(EditTagParams editTagParams);
  Future<Either<Failure, List<TagEntity>>> getAllTags();
  Future<Either<Failure, TagEntity>> deleteTag(TagEntity tag);
}
