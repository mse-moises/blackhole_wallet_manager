import 'package:blackhole_wallet_manager/core/error/failures.dart';
import 'package:blackhole_wallet_manager/core/usecases/usecase_interface.dart';
import 'package:blackhole_wallet_manager/features/tag/domain/entities/tag_entity.dart';
import 'package:blackhole_wallet_manager/features/tag/domain/repository/tag_repository_interface.dart';
import 'package:blackhole_wallet_manager/features/tag/domain/usecases/create_tag_usecase.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

class EditTagUseCase implements UseCase<TagEntity, EditTagParams> {
  final TagRepository repository;

  EditTagUseCase({required this.repository});
  @override
  Future<Either<Failure, TagEntity>> call(EditTagParams params) {
    return repository.editTag(params);
  }
}

class EditTagParams extends Equatable {
  final TagEntity originalTag;
  final TagEntity editedTag;

  const EditTagParams({required this.originalTag, required this.editedTag});

  @override
  List<Object?> get props => [originalTag, editedTag];
}
