import 'package:blackhole_wallet_manager/core/usecases/usecase_interface.dart';
import 'package:blackhole_wallet_manager/features/tag/domain/entities/tag_entity.dart';
import 'package:blackhole_wallet_manager/core/error/failures.dart';
import 'package:blackhole_wallet_manager/features/tag/domain/repository/tag_repository_interface.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

class CreateTagUseCase implements UseCase<TagEntity, CreateTagParams> {
  final TagRepository repository;

  CreateTagUseCase({required this.repository});

  @override
  Future<Either<Failure, TagEntity>> call(params) {
    return repository.createTag(params.title);
  }
}

class CreateTagParams extends Equatable {
  final String title;

  const CreateTagParams({required this.title});
  @override
  List<Object?> get props => [title];
}
