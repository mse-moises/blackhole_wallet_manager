import 'package:blackhole_wallet_manager/core/error/failures.dart';
import 'package:blackhole_wallet_manager/core/usecases/usecase_interface.dart';
import 'package:blackhole_wallet_manager/features/tag/domain/entities/tag_entity.dart';
import 'package:blackhole_wallet_manager/features/tag/domain/repository/tag_repository_interface.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

class DeleteTagUseCase implements UseCase<TagEntity, DeleteTagParams> {
  final TagRepository repository;

  DeleteTagUseCase({required this.repository});
  @override
  Future<Either<Failure, TagEntity>> call(DeleteTagParams params) {
    return repository.deleteTag(params.tag);
  }
}

class DeleteTagParams extends Equatable {
  final TagEntity tag;

  DeleteTagParams({required this.tag});

  @override
  List<Object?> get props => [tag];
}
