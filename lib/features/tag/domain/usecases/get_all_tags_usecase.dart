import 'package:blackhole_wallet_manager/core/error/failures.dart';
import 'package:blackhole_wallet_manager/core/usecases/usecase_interface.dart';
import 'package:blackhole_wallet_manager/features/tag/domain/entities/tag_entity.dart';
import 'package:blackhole_wallet_manager/features/tag/domain/repository/tag_repository_interface.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

class GetAllTagsUseCase implements UseCase<List<TagEntity>, GetAllTagsParams> {
  final TagRepository repository;

  GetAllTagsUseCase({required this.repository});

  @override
  Future<Either<Failure, List<TagEntity>>> call(GetAllTagsParams params) {
    return repository.getAllTags();
  }
}

class GetAllTagsParams extends Equatable {
  const GetAllTagsParams();
  @override
  List<Object?> get props => [];
}
