import 'package:blackhole_wallet_manager/features/tag/data/datasource/tag_local_datasource.dart';
import 'package:blackhole_wallet_manager/features/tag/data/model/tag_model.dart';
import 'package:blackhole_wallet_manager/features/tag/domain/entities/tag_entity.dart';
import 'package:blackhole_wallet_manager/core/error/failures.dart';
import 'package:blackhole_wallet_manager/features/tag/domain/repository/tag_repository_interface.dart';
import 'package:blackhole_wallet_manager/features/tag/domain/usecases/edit_tag_usecase.dart';
import 'package:dartz/dartz.dart';

class TagRepositoryImplementation implements TagRepository {
  final TagLocalDataSource localDataSource;

  TagRepositoryImplementation({required this.localDataSource});
  @override
  Future<Either<Failure, TagModel>> createTag(String title) async {
    try {
      return Right(await localDataSource.createTag(title));
    } catch (e) {
      return Left(DeviceFailure());
    }
  }

  @override
  Future<Either<Failure, List<TagEntity>>> getAllTags() async {
    try {
      return Right(await localDataSource.getAllTags());
    } catch (e) {
      return Left(DeviceFailure());
    }
  }

  @override
  Future<Either<Failure, TagEntity>> editTag(
      EditTagParams editTagParams) async {
    try {
      return Right(await localDataSource.editTag(editTagParams));
    } catch (e) {
      return Left(DeviceFailure());
    }
  }

  @override
  Future<Either<Failure, TagEntity>> deleteTag(TagEntity tag) async {
    try {
      return Right(await localDataSource.deleteTag(tag));
    } catch (e) {
      return Left(DeviceFailure());
    }
  }
}
