import 'package:blackhole_wallet_manager/features/tag/domain/entities/tag_entity.dart';

class TagModel extends TagEntity {
  const TagModel({required String title}) : super(title: title);
}
