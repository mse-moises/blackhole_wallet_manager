import 'package:blackhole_wallet_manager/features/tag/data/model/tag_model.dart';
import 'package:blackhole_wallet_manager/features/tag/domain/entities/tag_entity.dart';
import 'package:blackhole_wallet_manager/features/tag/domain/usecases/edit_tag_usecase.dart';

abstract class TagLocalDataSource {
  Future<TagModel> createTag(String title);
  Future<List<TagModel>> getAllTags();
  Future<TagModel> editTag(EditTagParams editTagParams);
  Future<TagModel> deleteTag(TagEntity tag);
}
