import 'package:blackhole_wallet_manager/App.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const BlackHoleApp());
}
